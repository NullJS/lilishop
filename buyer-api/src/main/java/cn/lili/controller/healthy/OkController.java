package cn.lili.controller.healthy;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.goods.entity.vos.CategoryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * OkController {@link }
 *
 * @author: J.S
 * @since: 2023/12/27
 */
@RestController
@Api(tags = "健康检查")
@RequestMapping("/buyer/check")
public class OkController {

    @ApiOperation(value = "是否健康")
    @GetMapping(value = "/")
    public ResultMessage<String> ok() {

        return ResultUtil.data("ok");
    }
}
