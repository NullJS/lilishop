package cn.lili.controller.passport.connect;


import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.security.token.Token;
import cn.lili.common.utils.UuidUtils;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.connect.entity.dto.AuthCallback;
import cn.lili.modules.connect.entity.dto.AuthResponse;
import cn.lili.modules.connect.entity.dto.ConnectAuthUser;
import cn.lili.modules.connect.request.AuthRequest;
import cn.lili.modules.connect.service.ConnectService;
import cn.lili.modules.connect.util.ConnectUtil;
import cn.lili.modules.member.service.MemberService;
import com.alibaba.fastjson.JSON;
import com.google.gson.JsonObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 买家端,web联合登录
 *
 * @author Chopper
 */
@Slf4j
@RestController
@Api(tags = "买家端,web联合登录")
@RequestMapping("/buyer/passport/connect/connect")
public class ConnectBuyerWebController {

    @Autowired
    private ConnectService connectService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private ConnectUtil connectUtil;


    @GetMapping("/login/web/{type}")
    @ApiOperation(value = "WEB信任登录授权,包含PC、WAP")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "登录方式:QQ,微信,微信_PC",
                    allowableValues = "QQ,WECHAT,WECHAT_PC", paramType = "path")
    })
    public ResultMessage<String> webAuthorize(@PathVariable String type, HttpServletResponse response) throws IOException {
        log.info(".webAuthorize 接受到请求 type:{}",type);
        AuthRequest authRequest = connectUtil.getAuthRequest(type);
        String authorizeUrl = authRequest.authorize(UuidUtils.getUUID());
        response.sendRedirect(authorizeUrl);
        log.info(".webAuthorize 处理成功 authorizeUrl:{}",authorizeUrl);
        return ResultUtil.data(authorizeUrl);
    }


    @ApiOperation(value = "信任登录统一回调地址", hidden = true)
    @GetMapping("/callback/{type}")
    public void callBack(@PathVariable String type, AuthCallback callback, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        log.info(".callBack 接受到回调 type:{} callback：{}", type, JSON.toJSONString(callback));
        connectUtil.callback(type, callback, httpServletRequest, httpServletResponse);
    }


    @ApiOperation(value = "信任登录统一回调地址", hidden = true)
    @GetMapping("/getAccessTokenByCode/{code}")
    public ResultMessage<Object> getAccessToken(@PathVariable String code) throws IOException {
        log.info(".callBack 接受到回调 code:{} ", code);
        return connectUtil.getAccessToken(code);
    }

    @ApiOperation(value = "信任登录响应结果获取")
    @GetMapping("/result")
    public ResultMessage<Object> callBackResult(String state) {
        log.info(".callBackResult state：{}" ,state);
        if (state == null) {
            throw new ServiceException(ResultCode.USER_CONNECT_LOGIN_ERROR);
        }
        ResultMessage<Object> result = connectUtil.getResult(state);
        log.info(".callBackResult result：{}",JSON.toJSONString(result));
        return result;
    }

    @ApiOperation(value = "APP-unionID登录")
    @PostMapping("/app/login")
    public ResultMessage<Token> unionLogin(@RequestBody ConnectAuthUser authUser, @RequestHeader("uuid") String uuid) {
        try {
            return ResultUtil.data(connectService.unionLoginCallback(authUser, uuid));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("unionID登录错误", e);
        }
        return null;
    }

}
