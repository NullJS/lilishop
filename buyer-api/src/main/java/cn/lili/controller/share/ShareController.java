package cn.lili.controller.share;

import cn.hutool.http.HttpUtil;
import cn.lili.cache.impl.RedisCache;
import cn.lili.common.enums.ResultCode;
import cn.lili.common.enums.ResultUtil;
import cn.lili.common.exception.ServiceException;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.payment.entity.enums.PaymentClientEnum;
import cn.lili.modules.payment.entity.enums.PaymentMethodEnum;
import cn.lili.modules.payment.kit.CashierSupport;
import cn.lili.modules.payment.kit.dto.PayParam;
import cn.lili.modules.payment.kit.params.dto.CashierParam;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 买家端,分享前的接口
 *
 * @author Chopper
 * @since 2020-12-18 16:59
 */
@Slf4j
@RestController
@Api(tags = "买家端,分享接口")
@RequestMapping("/buyer/share")
public class ShareController {


    @Autowired
    private RedisCache redisCache;

    @Value("${weixin.appid}")
    private String appId;

    @Value("${weixin.secret}")
    private String secret;


    /**
     * 网页授权
     * 通过code换取网页授权access_token（该AccessToken与基础AccessToken不一样）
     */
    public final static String WEB_GET_ACCESS_TOKEN = " https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

    public final static String WEB_GET_JSAPI_TICKET = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";

    /**
     * 网页授权
     * 获取用户详细信息（scope为 snsapi_userinfo）
     */
    public final static String WEB_GET_USER_INFO = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";



    /**
     * 获取access_token的值
     * @return
     */
    @GetMapping("/getToken")
    public String getToken() {
        String token = "";
        String url = WEB_GET_ACCESS_TOKEN.replace("APPID", appId).replace("APPSECRET", secret);
        String body = HttpUtil.createGet(url)
                .execute()
                .body();
        log.info("获取了token,返回数据 body:{} ", body);
        JSONObject object = JSON.parseObject(body);
        //获取token
        token = object.getString("access_token");
        //失效时间
        String expires_in = object.getString("expires_in");
        //将token值的值放在redis里面
        redisCache.put("gzh_access_token", token,7190L, TimeUnit.SECONDS);
        return token;
    }

    @GetMapping("/getOpenIdByCode")
    public String getOpenIdByCode(String code) {
        String url = WEB_GET_ACCESS_TOKEN.replace("APPID", appId).replace("SECRET", secret).replace("CODE", code);
        String body = HttpUtil.createGet(url)
                .execute()
                .body();
        log.info("获取了token,返回数据 body:{} ", body);
        JSONObject object = JSON.parseObject(body);
        //获取 openId
        String openId =  object.getString("openid");
        return openId;
    }

    /**
     * 获取jsapi_ticket
     * @return
     */
    @GetMapping("/getJsapiTicket")
    public String getJsapiTicket() {
        //获取redis里面的token
        Object access_token = redisCache.get("gzh_access_token");
        log.info("从Redis中获取的token: {}", access_token);
        if (access_token==null) {
            access_token = getToken();
        }
        log.info("实际使用的token:{}",access_token);
        String url = WEB_GET_JSAPI_TICKET.replace("ACCESS_TOKEN", access_token.toString());
        String body = HttpUtil.createGet(url)
                .execute()
                .body();
        log.info("获取了JsapiTicket,返回数据" + body);
        JSONObject object = JSON.parseObject(body);
        //获取ticket
        String ticket = object.getString("ticket");
        log.info("ticket:{}",ticket);
        //错误码
        Integer errcode = object.getInteger("errcode");
        if(errcode==0){
            //将ticket值的值放在redis里面
            redisCache.put("gzh_ticket", ticket,7190L,TimeUnit.SECONDS);
        }
        return ticket;
    }

    /**
     * 签名
     */
    @GetMapping("/getSing")
    public ResultMessage<JSONObject> getSing(String url){
        //从redis里面获取ticket
        Object ticket = redisCache.get("gzh_ticket");
        log.info("ticket:{}",ticket);
        if(ticket==null){
            ticket = getJsapiTicket();
        }
        log.info("实际使用的ticket:{}",ticket);
        Map<String, String> ret = WeChatUtils.sign(ticket.toString(), url);
        log.info("sing:{}",ret);
        JSONObject objectData = new JSONObject();
        for (Map.Entry entry : ret.entrySet()) {
            objectData.put(entry.getKey().toString(),entry.getValue());
        }
        objectData.put("appId", appId);
        return ResultUtil.data(objectData);
    }

    @GetMapping("/getSignInfo")
    public ResultMessage<JSONObject> getSignInfo(){
        getToken();
        getJsapiTicket();
        ResultMessage<JSONObject> sing = getSing("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx7ff43cdb712fc05b&redirect_uri=https://m.kuaikuai.fun/pages/cart/dfPay/sharePay&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect");
        return sing;
    }

    @GetMapping("/getOpenId")
    public ResultMessage<JSONObject> getOpenId(){
        getToken();
        getJsapiTicket();
        ResultMessage<JSONObject> sing = getSing("https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx7ff43cdb712fc05b&redirect_uri=https://m.kuaikuai.fun/pages/cart/dfPay/sharePay&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect");
        return sing;
    }


}
