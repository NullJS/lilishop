package cn.lili.controller.h5;

import cn.lili.common.enums.ResultUtil;
import cn.lili.common.security.token.Token;
import cn.lili.common.vo.ResultMessage;
import cn.lili.modules.goods.entity.vos.GoodsVO;
import cn.lili.modules.member.entity.dos.Member;
import cn.lili.modules.member.service.MemberService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.NotNull;
import java.nio.charset.Charset;
import java.util.Collections;

/**
 * TestController {@link }
 *
 * @author: J.S
 * @since: 2023/12/30
 */
@RestController
@Api(tags = "买家端,商品分类接口")
@RequestMapping("/h5")
@Slf4j
public class TestController {

    @ApiOperation(value = "微信验证", notes = "微信验证", httpMethod = "GET")
    @GetMapping(value = "/checkSignature", produces = "application/json;charset=utf-8")
//    @RequrieAuth(needLogin = false,noValidate = true)// //不需要sign验证
    public String checkSignature(String signature,String timestamp,String nonce,String echostr) {
        return echostr;//没有写验证，直接返回
    }

    @Value("${weixin.appid}")
    private String appId;

    @Value("${weixin.secret}")
    private String secret;
    @Value("${isXy}")
    private Boolean isXy;

    @Autowired
    private MemberService memberService;


    /**
     * 网页授权
     * 通过code换取网页授权access_token（该AccessToken与基础AccessToken不一样）
     */
    public final static String WEB_GET_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";

    /**
     * 网页授权
     * 获取用户详细信息（scope为 snsapi_userinfo）
     */
    public final static String WEB_GET_USER_INFO = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";


    /**
     * 网页授权
     * 通过code获取微信个人信息
     */
    @RequestMapping(value = "/getWxInfoByCode",method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public ResultMessage<JSONObject> getWxInfoByCode(@RequestParam(value = "code") String code){
        JSONObject resultObj  = null;
        String url = WEB_GET_ACCESS_TOKEN.replace("APPID", appId).replace("SECRET", secret).replace("CODE", code);
        log.info("WEB_GET_ACCESS_TOKEN:{}",url);
        JSONObject accessTokenObj = getJsonData(url);
        log.info("accessTokenObj={}", accessTokenObj);
        if(accessTokenObj != null && accessTokenObj.get("access_token") != null ){
            String accessToken = accessTokenObj.getString("access_token");
            String openId =  accessTokenObj.getString("openid");
            log.info("accessToken:{} openId:{}",accessToken,openId);
            if(accessToken != null){
                String webGetUserInfoUrl = WEB_GET_USER_INFO.replace("ACCESS_TOKEN", accessToken).replace("OPENID", openId);
                log.info("webGetUserInfoUrl:{}",webGetUserInfoUrl);
                resultObj = getJsonData(webGetUserInfoUrl);
                log.info("resultObj:{}",resultObj);
            }

            if (isXy){
                // 模拟注册用户，根据当前人的openId
                Member byMobile = memberService.findByMobile(openId);
                if (null != byMobile){
                    memberService.removeById(byMobile.getId());
                }
                Token register = memberService.register(openId, openId, openId);
                resultObj.put("localToken",register);
            }
        }
        // 根据openId注册用户
        return ResultUtil.data(resultObj);
    }

    /**
     * 发起get请求
     * @param url
     * @return
     */
    public JSONObject getJsonData(String url) {
        RestTemplate restTemplate = new RestTemplate();
        // 设置 HTTP 头部信息
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Accept-Charset", "UTF-8");
        // 创建 StringHttpMessageConverter 并设置编码方式为 UTF-8
        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        stringHttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        restTemplate.getMessageConverters().add(0, stringHttpMessageConverter);
        // 发送 GET 请求，并将响应映射为 JSONObject
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        // 获取响应中的 JSONObject
        JSONObject jsonObject = JSONObject.parseObject(responseEntity.getBody());
        return jsonObject;
    }


}
